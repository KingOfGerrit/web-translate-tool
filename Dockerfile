FROM python:3.10-slim

WORKDIR /app

COPY . .

VOLUME ["/app/data"]
VOLUME ["/app/config"]

ENV DATABASE_DIR=/app/data
ENV CONFIG_DIR=/app/config

RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "app.py"]

EXPOSE 5000
