from flask import Flask, request, jsonify, send_file, render_template
import sqlite3
import io
import requests
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
import yaml
import os

app = Flask(__name__)

CONFIG_PATH = os.path.join(os.path.dirname(__file__), 'config/config.yaml')
DB_PATH = os.path.join(os.path.dirname(__file__), 'data/words.db')

if not os.path.exists(CONFIG_PATH):
    with open(CONFIG_PATH, 'w+') as file:
        data = dict(
            deepl=dict(
                api_key='your_deepl_api_key',
                api_url='example'
            ),
            google=dict(
                api_key='your_google_api_key',
                api_url='example'
            )
        )
        yaml.dump(data, CONFIG_PATH, default_flow_style=False)
    print('Fist start are done')
    exit(0)

# Load configuration from YAML file
with open(CONFIG_PATH, 'r') as config_file:
    config = yaml.safe_load(config_file)

DEEPL_API_KEY = config['deepl']['api_key']
DEEPL_API_URL = config['deepl']['api_url']
GOOGLE_API_KEY = config['google']['api_key']
GOOGLE_API_URL = config['google']['api_url']


def get_word_analysis(word):
    try:
        headers = {
            'Content-Type': 'application/json'
        }
        data = {
            "contents": [
                {
                    "parts": [
                        {
                            "text": f"Could you please add a brief extended translation of these word '{word}'."
                                    f"This extended translation should be in Russian."
                                    f"This extended translation should be short in one sentence."
                        }
                    ]
                }
            ]
        }
        response = requests.post(f'{GOOGLE_API_URL}?key={GOOGLE_API_KEY}', headers=headers, json=data)
        response.raise_for_status()
        result = response.json()
        print(result)
        explanation = result['candidates'][0]['content']['parts'][0]['text'].strip()
        return explanation
    except requests.exceptions.RequestException as e:
        return f"Error: {e}"


def translate_with_deepl(text, target_lang):
    params = {
        'auth_key': DEEPL_API_KEY,
        'text': text,
        'target_lang': target_lang
    }
    response = requests.post(DEEPL_API_URL, data=params)
    try:
        result = response.json()
        return result['translations'][0]['text']
    except requests.exceptions.JSONDecodeError:
        print(f"Error: Unable to decode JSON response. Response text: {response.text}")
        raise


def translate_with_google_api(text, target_lang):
    try:
        headers = {
            'Content-Type': 'application/json'
        }
        data = {
            "contents": [
                {
                    "parts": [
                        {
                            "text": f"Translate the following text to {target_lang}: '{text}'"
                        }
                    ]
                }
            ]
        }
        response = requests.post(f'{GOOGLE_API_URL}?key={GOOGLE_API_KEY}', headers=headers, json=data)
        response.raise_for_status()
        result = response.json()
        translation = result['candidates'][0]['content']['parts'][0]['text'].strip()
        return translation
    except requests.exceptions.RequestException as e:
        return f"Error: {e}"


def init_db():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS words
                 (id INTEGER PRIMARY KEY, word TEXT UNIQUE, translation_ru TEXT, translation_uk TEXT, explanation TEXT, checked BOOLEAN DEFAULT 0)''')
    # Ensure 'checked' column exists
    try:
        c.execute('ALTER TABLE words ADD COLUMN checked BOOLEAN DEFAULT 0')
    except sqlite3.OperationalError:
        # Column already exists
        pass
    conn.commit()
    conn.close()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/list')
def list_words():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute("SELECT word, translation_ru, translation_uk, explanation, checked FROM words")
    words = c.fetchall()
    conn.close()
    words = [{'word': row[0], 'translation_ru': row[1], 'translation_uk': row[2], 'explanation': row[3],
              'checked': bool(row[4])} for row in words]
    return render_template('list.html', words=words)


@app.route('/translate', methods=['POST'])
def translate_word():
    data = request.json
    word = data['word']
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute("SELECT translation_ru, translation_uk, explanation, checked FROM words WHERE word=?", (word,))
    result = c.fetchone()
    if result:
        translation_ru, translation_uk, explanation, checked = result
        message = "This word is already present in the database."
    else:
        translation_ru = translate_with_deepl(word, 'RU')
        translation_uk = translate_with_deepl(word, 'UK')
        explanation = get_word_analysis(f'{word} - {translation_ru}')  # Use Google Gemini to get the explanation
        checked = False
        c.execute(
            "INSERT INTO words (word, translation_ru, translation_uk, explanation, checked) VALUES (?, ?, ?, ?, ?)",
            (word, translation_ru, translation_uk, explanation, checked))
        conn.commit()
        message = "The word has been added to the database."
    conn.close()
    return jsonify({'translation_ru': translation_ru, 'translation_uk': translation_uk, 'message': message})


@app.route('/words', methods=['GET'])
def get_words():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute("SELECT word, translation_ru, translation_uk, explanation, checked FROM words")
    words = [{'word': row[0], 'translation_ru': row[1], 'translation_uk': row[2], 'explanation': row[3],
              'checked': bool(row[4])} for row in c.fetchall()]
    conn.close()
    return jsonify({'words': words})


@app.route('/update_checked', methods=['POST'])
def update_checked():
    data = request.json
    word = data['word']
    checked = data['checked']
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute("UPDATE words SET checked=? WHERE word=?", (checked, word))
    conn.commit()
    conn.close()
    return jsonify({'message': 'Checked state updated'})


@app.route('/delete_word', methods=['POST'])
def delete_word():
    data = request.json
    word = data['word']
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute("DELETE FROM words WHERE word=?", (word,))
    conn.commit()
    conn.close()
    return jsonify({'message': 'Word deleted'})


@app.route('/export', methods=['GET'])
def export_words():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    export_type = request.args.get('type', 'all')

    if export_type == 'checked':
        c.execute("SELECT word, translation_ru, translation_uk, explanation FROM words WHERE checked=1")
    elif export_type == 'unchecked':
        c.execute("SELECT word, translation_ru, translation_uk, explanation FROM words WHERE checked=0")
    else:
        c.execute("SELECT word, translation_ru, translation_uk, explanation FROM words")

    words = c.fetchall()
    conn.close()

    output = io.BytesIO()
    pdf = canvas.Canvas(output, pagesize=letter)
    width, height = letter
    pdf.setTitle("Word List")

    # Register a font that supports Cyrillic characters
    pdfmetrics.registerFont(TTFont('FreeSerif', 'fonts/FreeSerif.ttf'))

    pdf.setFont('FreeSerif', 12)
    # pdf.drawString(30, height - 30, "Word List")
    # pdf.drawString(30, height - 50, f"Export Type: {export_type.capitalize()}")

    y_position = height - 70
    pdf.drawString(30, y_position, "Word")
    pdf.drawString(150, y_position, "Translation (Russian)")
    pdf.drawString(300, y_position, "Translation (Ukrainian)")
    pdf.drawString(450, y_position, "Explanation")

    y_position -= 20

    for word in words:
        if y_position < 40:  # Check if the page height is exceeded
            pdf.showPage()
            y_position = height - 30
        pdf.drawString(30, y_position, word[0])
        pdf.drawString(150, y_position, word[1])
        pdf.drawString(300, y_position, word[2])
        pdf.drawString(450, y_position, word[3])
        y_position -= 20

    pdf.save()
    output.seek(0)

    return send_file(output, download_name='words.pdf', as_attachment=True, mimetype='application/pdf')


if __name__ == '__main__':
    init_db()
    app.run(host="0.0.0.0", port=5000, debug=True)
